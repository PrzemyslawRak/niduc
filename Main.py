import ImageLoader
import Signal
import Tests
import random as rnd
from typing import List

def main():
    # binary = ImageLoader.img2binary()
    frequency = 200
    sample_freq = 6000
    binary_input = generate_random_binary_array(16)
    print(len(binary_input))

    print(Tests.bpsk_success_rate(binary_input, frequency, sample_freq, 1000, 2.5))
    Signal.show_plot(Signal.modulate_16qam(binary_input, frequency, sample_freq), frequency, sample_freq,
                     len(binary_input), bit_block_size=4)

    Tests.test_bpsk_with_plots(binary_input, frequency, sample_freq, 1.1)
    Tests.test_qpsk_with_plots(binary_input, frequency, sample_freq, 1.1)

    success_rate = 100.0
    noise = 1.0
    noise_tick = 0.1
    while success_rate == 100.0:
        success_rate = Tests.bpsk_success_rate(binary_input, frequency, sample_freq, 1000, noise)
        # print(noise)
        noise += noise_tick

    print("Pierwsze błędy przy BPSK zaczęły się pojawiać przy szumie " + noise.__str__())

    success_rate = 100.0
    noise = 1.0
    noise_tick = 0.1
    while success_rate == 100.0:
        success_rate = Tests.qpsk_success_rate(binary_input, frequency, sample_freq, 1000, noise)
        # print(noise)
        noise += noise_tick

    print("Pierwsze błędy przy QPSK zaczęły się pojawiać przy szumie " + noise.__str__())


def generate_random_binary_array(array_length):
    binary_array: List[int] = []

    for i in range(0, array_length):
        binary_array.append(rnd.choice([0, 1]))

    return binary_array


main()
